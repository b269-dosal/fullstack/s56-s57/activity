function countLetter(letter, sentence) {

    letter = letter.toLowerCase();
    sentence = sentence.toLowerCase();
    
    if (typeof letter !== 'string' || letter.length !== 1) {
        return undefined
    }

    let count = 0;

    for (let i = 0; i < sentence.length; i++) {
        if (sentence[i] === letter) {
            count++;
        }
    }

    return count;

    // Check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.    
}


function isIsogram(text) {

        text = text.toLowerCase();
        const charSet = new Set();

        for (let index = 0; index < text.length; index++) {
            const char = text[index];
    

            if (char !== ' ' && charSet.has(char)) {
                return false;
            }
            charSet.add(char);
        }

        return true;

    
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.

    
}
        
function purchase(age, price) {
        
        if (age < 13) {
          return undefined;
        }    
        else if ((age >= 13 && age <= 21) || age >= 65) {
         
          const discountedPrice = Math.round(price * 0.8);
      
          return discountedPrice.toString();
        }
        else {
          return Math.round(price).toString();
        }

}               
    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.
    


function findHotCategories(items) {
   
    let hotCategories = [];
  
    for (let i = 0; i < items.length; i++) {
      if (items[i].stocks === 0) {
        if (!hotCategories.includes(items[i].category)) {
          hotCategories.push(items[i].category);
        }
      }
    }
    return hotCategories;
  }
  

  const items = [
    { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' },
    { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' },
    { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' },
    { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' },
    { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }
  ];

  const hotCategories = findHotCategories(items);
  console.log("Hot Categories:", hotCategories); 
  


    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.

  
function findFlyingVoters(candidateA, candidateB) {

    let votersWhoVoteBoth = [];    
    for (let i = 0; i < candidateA.length; i++) {   
      if (candidateB.includes(candidateA[i])) {     
        if (!votersWhoVoteBoth.includes(candidateA[i])) {
            votersWhoVoteBoth.push(candidateA[i]);
        }
      }
    }

    return votersWhoVoteBoth;
  }
  
  const candidateA = ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m'];
  const candidateB = ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l'];
  
  const votersWhoVoteBoth = findFlyingVoters(candidateA, candidateB);
  console.log("Common Voters:", votersWhoVoteBoth);

    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
    


module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};